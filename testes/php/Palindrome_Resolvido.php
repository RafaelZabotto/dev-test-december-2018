<?php

/* ----------------- DESCRIÇÃO DO TESTE -----------------------*/

/*
Um palíndromo é uma palavra que pode ser lida  da mesma maneira da esquerda para direita, como ao contrário.
Exemplo: ASA.

Faça uma função que ao receber um input de uma palavra, string, verifica se é um palíndromo retornando verdadeiro ou falso.
O fato de um caracter ser maiúsculo ou mínusculo não deverá influenciar no resultado da função.

Exemplo: isPalindrome("Asa") deve retonar true.
*/



class Palindrome
{
	
    public static function isPalindrome($word)
    {

    	//Usa-se strtolower para deixar todas as letras minúculas
    	$palavra = strtolower($word);

    	//O PHP possui uma função de inverter string
    	$invertida = strrev($palavra);

    	if ($palavra == $invertida) {

    		echo '<pre>';
    		return TRUE;

    	}
        echo '<pre>';
    	return 0;
        
    }
  
}

echo Palindrome::isPalindrome('Deleveled');
echo Palindrome::isPalindrome('DnA');
echo Palindrome::isPalindrome('Ana');