<?php


/* ----------------- DESCRIÇÃO DO TESTE -----------------------*/

/*
Implemente uma função que ao receber um array associativo contendo arquivos e donos, retorne os arquivos agrupados por dono. 

Por exempolo, um array ["Input.txt" => "Jose", "Code.py" => "Joao", "Output.txt" => "Jose"] a função groupByOwners deveria retornar ["Jose" => ["Input.txt", "Output.txt"], "Joao" => ["Code.py"]].


*/

class FileOwners
{
    public static function groupByOwners($files)
    {	

    	foreach ($files as $f => $valor) {

    		//Separa-se os nomes e os indices em dois arrays dinâmicos;
    		$nomes[] = strtolower($valor);
    		$indices[] = $f;
    	}

    	//Faz a combinação dos dois array gerados, aonde os indices serão os nomes dos proprietários

    	$combinacao = array();

    	foreach ($nomes as $n => $i) {
        		$combinacao[$i][] = $indices[$n];
        }

        echo '<pre>';
        print_r($combinacao);

       //return NULL;
    }

}

$files = array
(
    "Input.txt" => "Jose",
    "Code.py" => "Joao",
    "Output.txt" => "Jose",
    "Click.js" => "Maria",
    "Out.php" => "maria",

);
var_dump(FileOwners::groupByOwners($files));